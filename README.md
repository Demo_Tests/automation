# DemoTest

This is Test Project. 

Clone the repo and run mvn clean install to run the project.

Runner classes location :- /Demo_Project/src/test/java/runners

Reports will be saved under target/site/serenity/

Open index.html to view the reports of both the UI and API Scenarios.
